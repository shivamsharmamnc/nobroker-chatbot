const express = require('express');
const app = express(); 
var kafka = require('kafka-node'),
    HighLevelConsumer = kafka.HighLevelConsumer,
    client = new kafka.Client();


function OnMessageReceive() {

    var request = require('request');
    var consumer = new HighLevelConsumer(
        client,
        [
            { topic: 'NbChat' }
        ],
        {
            groupId: "1234",
            autoCommit: true,
            autoCommitIntervalMs: 5000
        }
    );

    consumer.on('message', function (kafkaMessage) {
        
        console.log("Query : " + kafkaMessage.value);
        var form = {
            "contexts": [],
            "lang": "en",
            "query": kafkaMessage.value,
            "sessionId": "12345",
            "timezone": "America/New_York"
        };

        var formData = JSON.stringify(form);

        request({
            headers: {
                "Authorization": "Bearer b5246f3d3580404b974ed13b356a843f",
                "Content-Type": "application/json"
            },
            uri: 'https://api.dialogflow.com/v1/query?v=20150910',
            body: formData,
            method: 'POST'
          }, function (err, res, body) {
            body = JSON.parse(body);
            console.log(".|.|.|.|.|.");
            console.log("Intent : " + body.result.metadata.intentName);
            console.log("Score : " + body.result.score);
            console.log("Time stamp : " + body.timestamp);

            if (body.result.metadata.intentName == "Default Fallback Intent") {
                console.log("came in fallback.");
                return;
            }

            var req2 = require('request');
            var data = {
                "username": "falcon",
                "channel": "chat-bot",
                "icon_emoji": ":ghost:",
                "attachments": [
                  {
                    "footer": "falcon",
                    "color": "info",
                    "text": kafkaMessage.value,
                    "title": "Message",
                    "mrkdwn_in": [
                      "text",
                      "pretext"
                    ],
                    "fields": [
                      {
                        "short": "true",
                        "value": body.result.metadata.intentName,
                        "title": "DiagF Intent"
                      },
                      {
                        "short": "true",
                        "value": body.result.score,
                        "title": "DiagF score"
                      }
                    ],
                    "pretext": "Dialogue Flow Alert : Chat Bot",
                    "thumb_url": "https:\/\/d3snwcirvb4r88.cloudfront.net\/static\/img\/loadingIcon_2.png",
                    "footer_icon": "https:\/\/ca.slack-edge.com\/T04LKLU4S-U04M86361-04aa49c3c3ce-48"
                  }
                ]
            }
            
            req2({
                headers:{},
                uri:"https://hooks.slack.com/services/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",
                body:JSON.stringify(data),
                method:'POST'
            }, function(e, r, b){
                console.log("Slack status : " + b);
            });
            
          });        
    });

    return {
        close : function() {
            console.log('Shutting down consumer');
            consumer.close(false, function() {
                console.log('Consumer closed');
                console.log('Shutting down consumer client');
                client.close(function() {
                    console.log('Consumer Client closed');
                });
            });
        }
    };
};


OnMessageReceive();